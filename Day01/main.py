import numpy as np
import re
from itertools import islice
from numpy.core.defchararray import isnumeric


def answer1():
    arrayInput = open("input-test.txt", "r").read().split('\n')
    arrayInput = open("input.txt", "r").read().split('\n')
    numbers = []
    for line in arrayInput:
        number = '0'
        for char in line:
            if isnumeric(char):
                number = char
                break

        line = line[::-1]
        for char in line:
            if isnumeric(char):
                number += char

                break
        numbers.append(int(number))

    return np.array(numbers).sum()


def answer2():
    arrayInput = open("input-test.txt", "r").read().split('\n')
    arrayInput = open("input.txt", "r").read().split('\n')
    numbers = []

    stringNumbers = {
        'one': '1',
        'two': '2',
        'three': '3',
        'four': '4',
        'five': '5',
        'six': '6',
        'seven': '7',
        'eight': '8',
        'nine': '9',
    }

    # 54547 nope
    # arrayInput = ['ninenineninenine']
    for line in arrayInput:
        actions = []
        for stringNumber in stringNumbers:
            # index = line.find(stringNumber)
            indexes = [m.start() for m in re.finditer(f"(?={stringNumber})", line)]
            for index in indexes:
                if index != -1:
                    actions.append([stringNumbers[stringNumber], index])
        def func(key):
            return key[1]
        actions.sort(reverse=False,key=func)
        indexplus = 0
        for action in actions:
            num = action[0]
            index = action[1] + indexplus
            line = line[:index] + num + line[index:]
            indexplus += 1

        number = '0'
        for char in line:
            if isnumeric(char):
                number = char
                break

        line = line[::-1]

        for char in line:
            if isnumeric(char):
                number += char
                break

        numbers.append(int(number))

    return np.array(numbers).sum()


answer1 = answer1()
answer2 = answer2()

print(f"Answer 1: {answer1}")
print(f"Answer 2: {answer2}")
