import numpy as np
from itertools import islice

arrayInput = open("input-test.txt", "r").read().split('\n')
arrayInput = open("input.txt", "r").read().split('\n')

def answer1():
    arrayInput = open("input.txt", "r").read().split('\n')
    arrayInput = open("input-test.txt", "r").read().split('\n')

    games = []

    successfullGamesSum = 0;
    for line in arrayInput:
        def splitSemicolon(str):
            return str.split(',')

        game = line.split(':')[1].split(';')
        game = list(map(splitSemicolon, game))

        id = line.split(':')[0].split(' ')[1]
        success = True
        for subset in game:
            for play in subset:
                if play[0] == ' ':
                    play = play[1:]
                split = play.split(' ')
                count = split[0]
                color = split[1]
                if color == 'red' and int(count) > 12:
                    success = False
                if color == 'green' and int(count) > 13:
                    success = False
                if color == 'blue' and int(count) > 14:
                    success = False


        if success:
            successfullGamesSum += int(id)


    return successfullGamesSum
def answer2():
    arrayInput = open("input-test.txt", "r").read().split('\n')
    arrayInput = open("input.txt", "r").read().split('\n')

    result = 0;
    for line in arrayInput:
        def splitSemicolon(str):
            return str.split(',')

        game = line.split(':')[1].split(';')
        game = list(map(splitSemicolon, game))

        id = line.split(':')[0].split(' ')[1]
        print(f"Game {id}: ")
        redCount = 0
        greenCount = 0
        blueCount = 0
        for subset in game:
            for play in subset:
                if play[0] == ' ':
                    play = play[1:]
                split = play.split(' ')
                count = int(split[0])
                color = split[1]
                if color == 'red' and count > redCount:
                    redCount = int(count)
                if color == 'green' and count > greenCount:
                    greenCount = int(count)
                if color == 'blue' and count > blueCount:
                    blueCount = int(count)

        multi = redCount * greenCount * blueCount
        result += multi

    return result


answer1 = answer1()
answer2 = answer2()

print(answer2)
print(f"Answer 1: {answer1}")
print(f"Answer 2: {answer2}")