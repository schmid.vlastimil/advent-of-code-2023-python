import numpy as np
from itertools import islice

arrayInput = open("input-test.txt", "r").read().split('\n')
arrayInput = open("input.txt", "r").read().split('\n')

# ANSI escape codes for some colors
RED = '\033[91m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
BLUE = '\033[94m'
MAGENTA = '\033[95m'
CYAN = '\033[96m'
WHITE = '\033[97m'
RESET = '\033[0m'  # Resets the color to default

# 529539 nope
# 531932 nope
def answer1():
    arrayInput = open("input-test.txt", "r").read().split('\n')
    arrayInput = open("input.txt", "r").read().split('\n')
    directions = {
        'right-up': [1, -1],
        'right': [1, 0],
        'right-down': [1, 1],
        'down': [0, 1],
        'left-down': [-1, 1],
        'left': [-1, 0],
        'left-up': [-1, -1],
        'up': [0, -1],
    }


    numbersNear = []
    currX = 0
    currY = 0
    for line in arrayInput:
        number = ""
        isNearSymbol = False
        currX = 0
        for char in line:
            endOfNumber = False
            if char.isnumeric():
                # print()
                # print(f"ORIGIN X:{currX} Y:{currY} {BLUE}Char:{char}{RESET}")

                number += char
                # find symbol
                for direction in directions:
                    dirX = currX + directions[direction][0]
                    dirY = currY + directions[direction][1]
                    # is out of bounds
                    isOutOfBounds = False
                    if len(arrayInput) <= dirY:
                        isOutOfBounds = True
                    if isOutOfBounds or (dirX < 0 or dirX >= len(arrayInput[dirY])):
                        isOutOfBounds = True
                    if isOutOfBounds or (dirY < 0 or dirY >= len(arrayInput)):
                        isOutOfBounds = True
                    if isOutOfBounds:
                        # print(f"FOUND X:{dirX} Y:{dirY} - Char:X Dir:{direction}")
                        continue


                    charFind = arrayInput[dirY][dirX]


                    if (not charFind.isnumeric()) and charFind != '.':
                        isNearSymbol = True
                        # print(f"FOUND X:{dirX} Y:{dirY} - {RED}Char:{charFind}{RESET} Dir:{direction}")
                    else:
                        ff = 'ff'
                        # print(f"FOUND X:{dirX} Y:{dirY} - Char:{RESET}{charFind}{RESET} Dir:{direction}")
            else:
                endOfNumber = True



            if endOfNumber or len(line) == currX+1:
                if isNearSymbol:
                    # print(f"Number Near: {GREEN}{number}{RESET}")
                    numbersNear.append(int(number))
                isNearSymbol = False
                number = ""


            currX += 1

        currY += 1
    # print(numbersNear)
    return np.array(numbersNear).sum()


# 78740604 nope
def answer2():
    arrayInput = open("input-pedryx.txt", "r").read().split('\n')
    arrayInput = open("input-test.txt", "r").read().split('\n')
    arrayInput = open("input.txt", "r").read().split('\n')
    directions = {
        'right-up': [1, -1],
        'right': [1, 0],
        'right-down': [1, 1],
        'down': [0, 1],
        'left-down': [-1, 1],
        'left': [-1, 0],
        'left-up': [-1, -1],
        'up': [0, -1],
    }


    gears = []
    currX = 0
    currY = 0
    for line in arrayInput:
        isNearSymbol = False
        currX = 0
        for char in line:
            endOfNumber = False
            if char == '*':
                # print()
                # print(f"ORIGIN X:{currX} Y:{currY} {BLUE}Char:{char}{RESET}")

                gear = []
                # find symbol
                for direction in directions:
                    dirX = currX + directions[direction][0]
                    dirY = currY + directions[direction][1]
                    # is out of bounds
                    isOutOfBounds = False
                    if len(arrayInput) <= dirY:
                        isOutOfBounds = True
                    if isOutOfBounds or (dirX < 0 or dirX >= len(arrayInput[dirY])):
                        isOutOfBounds = True
                    if isOutOfBounds or (dirY < 0 or dirY >= len(arrayInput)):
                        isOutOfBounds = True
                    if isOutOfBounds:
                        # print(f"FOUND X:{dirX} Y:{dirY} - Char:X Dir:{direction}")
                        continue


                    charFind = arrayInput[dirY][dirX]


                    if charFind.isnumeric() and charFind != '.':
                        gear.append([dirX, dirY])
                        isNearSymbol = True
                        # print(f"FOUND X:{dirX} Y:{dirY} - {RED}Char:{charFind}{RESET} Dir:{direction}")
                    else:
                        f = 'f'
                        # print(f"FOUND X:{dirX} Y:{dirY} - Char:{RESET}{charFind}{RESET} Dir:{direction}")


                endOfNumber = True



            if endOfNumber or len(line) == currX+1:
                if isNearSymbol and len(gear) > 1:
                    gears.append(gear)
                isNearSymbol = False
                number = ""


            currX += 1

        currY += 1


    gearRatioNumbers = []
    for gear in gears:
        numbers = []
        for numberPosition in gear:
            isStartPosition = False
            startModX = 0
            while not isStartPosition:
                print(numberPosition[0] - startModX)
                if (numberPosition[0] - startModX) < 0:
                    startModX -= 1
                    break
                char = arrayInput[numberPosition[1]][numberPosition[0] - startModX]
                if not char.isnumeric():
                    startModX -= 1
                    isStartPosition = True
                else:
                    startModX += 1

            isEndPosition = False
            number = ""
            while not isEndPosition:
                if len(arrayInput[numberPosition[1]]) <= (numberPosition[0] - startModX):
                    break
                char = arrayInput[numberPosition[1]][numberPosition[0] - startModX]
                if not char.isnumeric():
                    isEndPosition = True
                else:
                    number += char
                    startModX -= 1

            numbers.append(number)

        unique_elements = set(numbers)
        unique_list = list(unique_elements)
        gearRatioNumbers.append(unique_list)

    print(gearRatioNumbers)
    result = 0
    for gearRatioNumber in gearRatioNumbers:
        mult = 0
        if len(gearRatioNumber) == 2:
            mult = int(gearRatioNumber[0]) * int(gearRatioNumber[1])
        else:
            # mult = int(gearRatioNumber[0]) * int(gearRatioNumber[0])
            ll = 'f'

        result += mult

    print(gearRatioNumbers)
    return result


answer1 = answer1()
answer2 = answer2()

print(answer2)
print(f"Answer 1: {answer1}")
print(f"Answer 2: {answer2}")
