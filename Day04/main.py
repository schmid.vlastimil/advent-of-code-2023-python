import numpy as np
from itertools import islice

arrayInput = open("input-test.txt", "r").read().split('\n')
arrayInput = open("input.txt", "r").read().split('\n')

def answer1():
    arrayInput = open("input-test.txt", "r").read().split('\n')
    arrayInput = open("input.txt", "r").read().split('\n')

    points = 0
    for line in arrayInput:
        parsed = line.split(':')[1].split('|')
        winingNumbersPre = parsed[0].split(' ')
        winingNumbers = []
        for num in winingNumbersPre:
            if num != '':
                winingNumbers.append(num)

        gameNumbersPre = parsed[1].split(' ')
        gameNumbers = []
        for num in gameNumbersPre:
            if num != '':
                gameNumbers.append(num)

        winningNumbersCount = 0
        for winingNumber in winingNumbers:
            if winingNumber in gameNumbers:
                winningNumbersCount += 1

        if winningNumbersCount != 0:
            tempPoints = pow(2, winningNumbersCount - 1)
        else:
            tempPoints = 0

        points += tempPoints
    return points

class TestC:
    # A class variable, shared by all instances
    static_var = 0
    cards = []
    def increment_static_var(self):
        # Access and modify the class variable
        TestC.static_var += 1


for i in range(0,7):
    TestC.cards.append(0)
def answer2(startAtLine = 0, endAtLine = 300):
    arrayInput = open("input.txt", "r").read().split('\n')
    arrayInput = open("input-test.txt", "r").read().split('\n')

    totalScratchCards = 0
    lineNumber = -1
    for line in arrayInput:
        lineNumber += 1

        if lineNumber < startAtLine:
            continue


        if endAtLine <= lineNumber:
            break

        if TestC.static_var == 20:
            dd = 'd'

        print(startAtLine, endAtLine)
        print(TestC.cards)
        totalScratchCards += 1
        TestC.static_var += 1
        TestC.cards[lineNumber] += 1

        parsed = line.split(':')[1].split('|')
        winingNumbersPre = parsed[0].split(' ')
        winingNumbers = []
        for num in winingNumbersPre:
            if num != '':
                winingNumbers.append(num)

        gameNumbersPre = parsed[1].split(' ')
        gameNumbers = []
        for num in gameNumbersPre:
            if num != '':
                gameNumbers.append(num)

        winningNumbersCount = 0
        for winingNumber in winingNumbers:
            if winingNumber in gameNumbers:
                winningNumbersCount += 1
        print(f'Total:{totalScratchCards} Card:{startAtLine} Win:{winningNumbersCount} Step:{TestC.static_var}')


        for i in range(0, winningNumbersCount):
            fromLine = lineNumber + i + 1
            toLine = winningNumbersCount + lineNumber + i
            if TestC.static_var == 19:
                dd='d'
            print(f'from:{fromLine} to:{toLine}')
            totalScratchCards += answer2(fromLine, toLine)



    return totalScratchCards


def answer3():
    arrayInput = open("input-test.txt", "r").read().split('\n')
    arrayInput = open("input.txt", "r").read().split('\n')

    cardsArray = {} # number, copies, wins
    for line in arrayInput:
        cardIndex, numbers = line.split(':')
        cardIndex = int(cardIndex.split()[-1])
        split = numbers.split('|')
        winNumbers = split[0].split()
        gameNumbers = split[1].split()

        winCount = 0
        for winNumber in winNumbers:
            if winNumber in gameNumbers:
                winCount += 1
        cardsArray[cardIndex] = [cardIndex, 1, winCount]

    returnVal = 0
    for cardIndex in cardsArray.keys():
        for i in range(cardsArray[cardIndex][1]):
            # all wins loop
            for winIndex in range(cardsArray[cardIndex][2]):
                cardsArray[cardIndex + winIndex + 1][1] += 1
        returnVal += cardsArray[cardIndex][1]

    print(cardsArray)

    return returnVal

answer1 = answer1()
answer2 = answer3()

print(TestC.cards)
# 1,
print(f"Answer 1: {answer1}")
print(f"Answer 2: {answer2}")