import numpy as np
from itertools import islice

arrayInput = open("input-test.txt", "r").read().split('\n')
arrayInput = open("input.txt", "r").read().split('\n')

def answer1():
    arrayInput = open("input-test.txt", "r").read().split('\n')
    arrayInput = open("input.txt", "r").read().split('\n')

    times = arrayInput[0].split()[1:]
    distances = arrayInput[1].split()[1:]
    waysTotal = 1
    for index, time in enumerate(times):
        time = int(time)
        distance = int(distances[index])
        waysTemp = 0
        for timeStep in range(1, time):
            if (timeStep * (time - timeStep)) > distance:
                waysTemp += 1
        waysTotal *= waysTemp

    return waysTotal
def answer2():
    arrayInput = open("input.txt", "r").read().split('\n')
    arrayInput = open("input-test.txt", "r").read().split('\n')


    times = [53837288]
    distances = [333163512891532]
    waysTotal = 1
    for index, time in enumerate(times):
        time = int(time)
        distance = int(distances[index])
        waysTemp = 0
        for timeStep in range(1, time):
            if (timeStep * (time - timeStep)) > distance:
                waysTemp += 1
        waysTotal *= waysTemp

    return waysTotal

answer1 = answer1()
answer2 = answer2()

print(answer2)
print(f"Answer 1: {answer1}")
print(f"Answer 2: {answer2}")