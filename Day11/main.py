import numpy as np
from itertools import islice
from itertools import combinations

def answer1():
    return answer(2)
def answer2():
    return answer(1_000_000)

def answer(expansionRate = 2):
    arrayInput = open("input-test.txt", "r").read().split('\n')
    arrayInput = open("input.txt", "r").read().split('\n')

    # creating galaxy map
    galaxyMap = list()
    for line in arrayInput:
        tempArr = list()
        for char in line:
            tempArr.append(char)
        galaxyMap.append(tempArr)

    # extracting galaxies
    positionsGalaxies = list()
    for y, line in enumerate(galaxyMap):
        for x, symbol in enumerate(line):
            if symbol == "#":
                positionsGalaxies.append((x, y))

    # Expand map
    # Rows
    emptyRowIndicies = list()
    for rowIndex, row in enumerate(galaxyMap):
        isEmpty = False
        for j, char in enumerate(row):
            if char == '.':
                isEmpty = True
            else:
                isEmpty = False
                break
        if isEmpty:
            emptyRowIndicies.append(rowIndex)

    # Columns
    emptyColIndicies = []
    for colIndex in range(len(galaxyMap[0])):
        column = list()
        for row in range(len(galaxyMap)):
            column.append(galaxyMap[row][colIndex])

        isEmpty = False
        for j, char in enumerate(column):
            if char == '.':
                isEmpty = True
            else:
                isEmpty = False
                break
        if isEmpty:
            emptyColIndicies.append(colIndex)


    extendedGalaxies = list()

    for galaxy in positionsGalaxies:
        x = galaxy[0]
        y = galaxy[1]

        count = 0
        for indexTemp in emptyColIndicies:
            if indexTemp < x:
                count += 1
        x += (expansionRate - 1) * count

        count = 0
        for indexTemp in emptyRowIndicies:
            if indexTemp < y:
                count += 1
        y += (expansionRate - 1) * count

        extendedGalaxies.append((x, y))

    pairedGalaxies = combinations(extendedGalaxies, 2)

    # Measuring distance
    sum = 0
    for pair in list(pairedGalaxies):
        galaxy1 = pair[0]
        galaxy2 = pair[1]
        menhaton = abs(galaxy2[0] - galaxy1[0]) + abs(galaxy2[1] - galaxy1[1])
        sum += menhaton

    return sum

answer1 = answer1()
answer2 = answer2()

print(answer2)
print(f"Answer 1: {answer1}")
print(f"Answer 2: {answer2}")