import numpy as np
from itertools import islice
import time
from functools import cache

arrayInput = open("input-test.txt", "r").read().split('\n')
arrayInput = open("input.txt", "r").read().split('\n')


def answer1():
    arrayInput = open("input-test.txt", "r").read().split('\n')
    arrayInput = open("input.txt", "r").read().split('\n')

    count = 0
    for line in arrayInput:
        split = line.split(' ')
        counts = tuple(map(int, split[1].split(',')))
        count += countCombinations(split[0], counts)

    return count


def answer2():
    arrayInput = open("input-test.txt", "r").read().split('\n')
    arrayInput = open("input.txt", "r").read().split('\n')

    count = 0
    lineNumber = 0
    timeFromStart = time.time()
    for line in arrayInput:
        # print(f'Line: {lineNumber}')
        # timeFromStartEnd = time.time()
        # print(f"Total Time: {timeFromStartEnd - timeFromStart}")

        split = line.split(' ')
        stringTemp = split[0]
        countsTemp = tuple(map(int, split[1].split(',')))

        string = ''
        counts = tuple()
        for i in range(5):
            if i != 0:
                string += '?'
            string += stringTemp

            counts += countsTemp

        currentCount = countCombinations(string, counts)
        # print(f'currentCount: {currentCount}')
        count += currentCount
        # print(f'Total: {count}')
        lineNumber += 1
    return count

@cache
def countCombinations(line, counts: tuple):
    countFinal = 0

    if len(counts) == 0:
        isAllDone = True  # is all spring all unkown

        for char in line:
            if char == '#':
                isAllDone = False

        if isAllDone:
            return 1
        return 0

    currentCount = counts[0]
    remainingCounts = counts[1:]
    totalFreeSpaces = sum(remainingCounts) + len(remainingCounts)
    steps = len(line) - totalFreeSpaces - currentCount + 1
    for dotCount in range(steps):
        stringAdd = ''
        for i in range(dotCount):
            stringAdd += '.'
        for i in range(currentCount):
            stringAdd += '#'
        stringAdd += "."

        isDone = False
        for char0, char1 in zip(line, stringAdd):
            if not (char1 == char0 or char0 == "?"):
                isDone = True

        if not isDone:
            countFinal += countCombinations(line[len(stringAdd):], remainingCounts)
    return countFinal


answer1 = answer1()
answer2 = answer2()

print(f"Answer 1: {answer1}")
print(f"Answer 2: {answer2}")
