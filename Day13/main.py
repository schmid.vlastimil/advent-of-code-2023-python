import numpy as np
from itertools import islice
from time import perf_counter

arrayInput = open("input-test.txt", "r").read().split('\n')
arrayInput = open("input.txt", "r").read().split('\n')

def profiler(method):
    def wrapper_method(*arg, **kw):
        t = perf_counter()
        ret = method(*arg, **kw)
        print("Method " + method.__name__ + " took : " + "{:2.5f}".format(perf_counter() - t) + " sec")
        return ret

    return wrapper_method

@profiler
def answer1():
    matrixes = getMatrixes()
    result = 0
    for matrix in matrixes:
        # matrix = matrixes[1]
        binaryArrRows = matrixToBinaryRows(matrix)
        transMatrix = list(map(list, zip(*matrix)))
        binaryArrCols = matrixToBinaryRows(transMatrix)
        pointOfReflectionRows = findMiddleOfReflection(binaryArrRows)
        pointOfReflectionCols = findMiddleOfReflection(binaryArrCols)
        if pointOfReflectionRows == -1:

            print('')
        else:
            result += pointOfReflectionRows * 100
            # print(pointOfReflectionRows)
        if pointOfReflectionCols == -1:
            dd = 'd'
        else:
            result += pointOfReflectionCols

            # print(pointOfReflectionCols)


    return result

@profiler
def answer2():
    matrixes = getMatrixes()
    result = 0
    for matrix in matrixes:
        # matrix = matrixes[1]
        binaryArrRows = matrix
        transMatrix = list(map(list, zip(*matrix)))
        binaryArrCols = transMatrix
        pointOfReflectionRows = findMiddleOfReflection(binaryArrRows)
        pointOfReflectionCols = findMiddleOfReflection(binaryArrCols)
        if pointOfReflectionRows == -1:

            print('')
        else:
            result += pointOfReflectionRows * 100
            # print(pointOfReflectionRows)
        if pointOfReflectionCols == -1:
            dd = 'd'
        else:
            result += pointOfReflectionCols

            # print(pointOfReflectionCols)


    return result


    return 0


def findMiddleOfReflection(arr):
    # print(f'Array: {arr}')
    for pointOfReflection in range(1, len(arr)):
        inSeries = []
        for increment in range(1, len(arr)):
            if (
                    not isOutOfBounds(arr, pointOfReflection - increment) and not isOutOfBounds(arr, pointOfReflection + increment - 1) and
                    arr[pointOfReflection - increment] == arr[pointOfReflection + increment - 1]
            ):
                inSeries.append(arr[pointOfReflection - increment])
            else:
                break
        if len(inSeries) != 0:
            if isOutOfBounds(arr, pointOfReflection - increment) or isOutOfBounds(arr, pointOfReflection + increment - 1):
                # print(inSeries)
                return pointOfReflection
    return -1

def isOutOfBounds(array, index):
    return index < 0 or index >= len(array)
def matrixToBinaryRows(matrix):
    binaryArrRows = []
    for row in matrix:
        binaryNumber = ''
        for char in row:
            if char == '.':
                binaryNumber += '0'
            else:
                binaryNumber += '1'
        binaryArrRows.append(int(binaryNumber, 2))
    return binaryArrRows

def getMatrixes():
    arrayInput = open("input.txt", "r").read().split('\n')
    arrayInput = open("input-test.txt", "r").read().split('\n')

    matrixes = []
    matrix = []
    for line in arrayInput:
        if line == '':
            matrixes.append(matrix)
            matrix = []
        else:
            matrix.append(list(line))
    matrixes.append(matrix)

    return matrixes

answer1 = answer1()
answer2 = answer2()

print(f"Answer 1: {answer1}")
print(f"Answer 2: {answer2}")