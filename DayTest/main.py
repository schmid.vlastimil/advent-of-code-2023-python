import numpy as np
from itertools import islice

arrayInput = open("input-test.txt", "r").read().split('\n')
arrayInput = open("input.txt", "r").read().split('\n')

elfCals = []
curr = 0
for line in arrayInput:
    if line == '':
        elfCals.append(curr)
        curr = 0 # reset
    else:
        curr += int(line)



answer1 = np.array(elfCals).max()
elfCals.sort(reverse=True)
answer2 = np.array(list(islice(elfCals, 3))).sum()

print(answer2)
print(f"Answer 1: {answer1}")
print(f"Answer 2: {answer2}")